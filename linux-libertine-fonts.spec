%global fontname            linux-libertine
%global prio_libertine      60
%global prio_biolinum       61
%global fontconf_libertine  %{prio_libertine}-%{fontname}-libertine.conf
%global fontconf_biolinum   %{prio_biolinum}-%{fontname}-biolinum.conf
%global fontconf_metrics    29-%{fontname}-metrics-alias.conf
%global archivename         LinLibertine
%global posttag             2012_07_02

%define common_desc                                                     \
The Linux Libertine Open Fonts are a TrueType font family for practical \
use in documents.  They were created to provide a free alternative to   \
proprietary standard fonts.

Name:           %{fontname}-fonts
Version:        5.3.0
Release:        1.%{posttag}
Summary:        Linux Libertine Open Fonts

License:        GPL-2.0-or-later WITH Font-exception-2.0 OR OFL-1.1
URL:            http://linuxlibertine.sf.net
Source0:        http://download.sourceforge.net/sourceforge/linuxlibertine/LinLibertineOTF_%{version}_%{posttag}.tgz
Source1:        %{name}-libertine-fontconfig.conf
Source2:        %{name}-biolinum-fontconfig.conf
Source3:        %{name}-libertine-metrics-alias-fontconfig.conf
Source4:        libertine.metainfo.xml
Source5:        biolinum.metainfo.xml

BuildArch:      noarch
BuildRequires:  fontpackages-devel libappstream-glib
Requires:       %{name}-common = %{version}-%{release}

%description
%common_desc

This package contains Serif fonts.

%package -n %{fontname}-biolinum-fonts
Summary:        Sans-serif fonts from Linux Libertine Open Fonts
Requires:       %{name}-common = %{version}-%{release}

%description -n %{fontname}-biolinum-fonts
%common_desc

This package contains Sans fonts.

%package common
Summary:        Common files for Linux Libertine Open Fonts
Requires:       fontpackages-filesystem

%description common
%common_desc

This package consists of files used by other %{name} packages.

%prep
%setup -q -c
sed -i -e 's/\r//' OFL-1.1.txt

%build

%install
install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.otf %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE1} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf_libertine}
install -m 0644 -p %{SOURCE2} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf_biolinum}
install -m 0644 -p %{SOURCE3} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf_metrics}

for fconf in %{fontconf_libertine} %{fontconf_metrics} %{fontconf_biolinum}; do
    ln -s %{_fontconfig_templatedir}/$fconf \
          %{buildroot}%{_fontconfig_confdir}/$fconf
done

# Add AppStream metadata
install -Dm 0644 -p %{SOURCE4} \
        %{buildroot}%{_metainfodir}/libertine.metainfo.xml
install -Dm 0644 -p %{SOURCE5} \
        %{buildroot}%{_metainfodir}/biolinum.metainfo.xml

%check
appstream-util validate-relax --nonet %{buildroot}%{_metainfodir}/*.metainfo.xml

%files common
%license GPL.txt LICENCE.txt OFL-1.1.txt
%doc Bugs.txt ChangeLog.txt Readme-TEX.txt README

%_font_pkg -f %{fontconf_libertine} LinLibertine*.otf
%{_metainfodir}/libertine.metainfo.xml

%{_fontconfig_templatedir}/%{fontconf_metrics}
%{_fontconfig_confdir}/%{fontconf_metrics}

%_font_pkg -n biolinum -f %{fontconf_biolinum} LinBiolinum*.otf
%{_metainfodir}/biolinum.metainfo.xml

%changelog
* Wed Sep 06 2023 Darssin <2020303249@mail.nwpu.edu.cn> - 5.3.0-1.2012_07_02
- Package init
